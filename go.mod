module go.bug.st/serial.v1

go 1.16

require (
	github.com/adrianmo/go-nmea v1.3.0
	github.com/gorilla/mux v1.8.0
	github.com/mattn/go-sqlite3 v1.14.8
	github.com/olebedev/config v0.0.0-20190528211619-364964f3a8e4
	go.bug.st/serial v1.3.1
)
