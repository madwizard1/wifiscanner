FROM golang:1.16.6-alpine3.14 as build

WORKDIR /go/src/app

RUN apk add build-base git
ADD go.mod /go/src/app
ADD go.sum /go/src/app
RUN go mod download
ADD *.go /go/src/app/
ADD templates /go/src/app
ADD pub /go/src/app
ADD Makefile /go/src/app
RUN make intel

FROM alpine:3.14

COPY --from=build /go/src/build/server /usr/local/bin/

ENTRYPOINT ["/usr/local/bin/server"]
